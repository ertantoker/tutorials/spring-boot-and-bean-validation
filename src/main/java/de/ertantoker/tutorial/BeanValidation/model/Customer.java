package de.ertantoker.tutorial.BeanValidation.model;


import de.ertantoker.tutorial.BeanValidation.validation.Phone;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

public class Customer {

    @NotBlank
    private String firstName;

    @NotBlank
    @Phone
    private String phoneNumber;

    public Customer() {

    }

    public Customer(String firstName, String phoneNumber) {
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
